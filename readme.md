# Devoir maison sur la semaine de cours CI/CD

Ce qui va etre mis en place 

* Un workflow Git (Git flow ou Github flow)
* Les templates de merge request et d’issues
* Docker, ceci est un projet utilisant Symfony vous aurez donc besoin de plusieurs service
	* PHP-FPM
	* Nginx
	* MySQL/MariaDB
* Un readme
* Un Makefile
* Une CI (gitlab-ci) qui devra automatiser au minimum:
* Le linting de vos Dockerfile
* Le linting du code source (avec la configuration de l’outil, pour rappel PHP-cs-fixer)
* Les tests unitaires
* Uniquement sur master, la création de vos images embarquant l’intégralité du code source et leur publication sur le registry de gitlab (l’objectif étant de pouvoir déployer les images en production sans avoir à faire de partage de volumes)

----------------------------------------------------

## Problèmes rencontrés 

- Impossible de voir resultat pourtant j'ai bien run et build :/
- Je n'ai pas su implémenter hadolint au projet mais j'ai vérifié sur la page web 
