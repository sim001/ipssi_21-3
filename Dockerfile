
FROM php:7.4-cli
COPY . /usr/src/myapp
WORKDIR /usr/src/myapp
CMD [ "php", "./your-script.php" ]

FROM nginx:1.17
COPY static-html-directory /usr/share/nginx/html


